class Fixnum

    ONES = { 0 => "zero", 1 => "one", 2 => "two", 3 => "three", 4 => "four",
      5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine" }
    TEENS = { 10 => "ten", 11=> "eleven", 12 => "twelve", 13 => "thirteen",
      14 => "fourteen", 15 => "fifteen", 16 => "sixteen", 17 => "seventeen",
      18 => "eighteen", 19 => "nineteen" }
    TENS = { 20 => "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty",
      60 => "sixty", 70 => "seventy", 80 => "eighty", 90 => "ninety" }
    MAGNITUDES = {
      100 => "hundred",
      1000 => "thousand",
      1000000 => "million",
      1000000000 => "billion",
      1000000000000 => "trillion"
    }
    def mags
      arr = []
      MAGNITUDES.select {|k,v| arr << k if k <= self}
      arr.last
    end

    def num_arr
      self.to_s.chars.map { |el| el.to_i }
    end

    def tens(number)
      last_digit = (number.num_arr)[-1]

      if last_digit == 0
        TENS[number - last_digit]
      else
        TENS[number - last_digit] + " " + ONES[last_digit]
      end
    end

    def in_words
      selfmags = self.mags
      selfarr = self.num_arr
      finalwords = []
      num = self
      if self < 10
        ONES[self]
      elsif self < 20
        TEENS[self]
      elsif self < 100
        tens(self)
      else
     magnitude = mags
     magnitude_words = (self / magnitude).in_words + " " + MAGNITUDES[magnitude]
     if (self % magnitude) != 0
       magnitude_words + " " + (self % magnitude).in_words
     else
       magnitude_words
      end
      end
    end
end
